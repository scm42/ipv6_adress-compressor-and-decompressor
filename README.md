# ipv6_adress-compressor-and-decompressor

ipv6_adress-compressor-and-decompressor is a Python program which compress and decompress ipv6 addresses.

```bash
python ipv6_compression.py
```
## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)