
# nombre de zero dans un block

def number_zero(block:str):
    return block.count('0')

# supprime les zero present au debut de la chaine
def remove_zero(block:str):
     
    b = ''
    n = len(block)
    
    if n > 1:
       if block[0] == '0':
           i = 1
           while i < n-1 and block[i] == '0': i += 1
               
           for i in range(i,n):
               b+=block[i]      
           return b             
       else:
           return block
    
    return block

# ajoute les zeros au debut d'une chaine de caractère a l'origine de longueur 4
def add_zero(block:str):
    b = ''
    n = len(block)
    
    if n < 4:
        for i in range(4-n):
            b+='0'
        b+=block
        
        return b
        
    return block

# supprime les zeros present au debut de chaque bloc d'une addresse ipv6
def remove_leading_zeros(ipv6_address:str):
    
    i = 0
    blocks = [remove_zero(b) for b in ipv6_address.split(':')]
    n = len(blocks)
    b = ''
    
    for i in range(n-1):
        b+=blocks[i]
        b+=':'
    
    return b+blocks[-1]

# ajoute les zeros au debut de chaque bloc d'une addresse ipv6 compresser
def add_leading_zeros(ipv6_address:str):
    
    i = 0
    blocks = [add_zero(b) for b in ipv6_address.split(':')]
    n = len(blocks)
    b = ''
    
    for i in range(n-1):
        b+=blocks[i]
        b+=':'
    
    return b+blocks[-1]

# retourne l'indice de debut et de fin de zeros continue dans une addresse ipv6
def zero_indices(ipv6_address:str):
    
    blocks = ipv6_address.split(':')
    n = len(blocks)
    indices = []
    
    for i in range(n): 
        if (number_zero(blocks[i]) == 4) or (blocks[i] == '0'):
            indices.append(i)
                   
    k = len(indices)
    m = dict()

    for i in range(k):
        m.__setitem__(indices[i],[indices[i]])
        for j in range(k):
            if indices[i] - indices[j] in [-1,1]:
                l = m.__getitem__(indices[i])
                l.append(indices[j])
                m.__setitem__(indices[i],l)
                
    
    d = 0
    f = []
    for i in range(k):
        l = m.__getitem__(indices[i])
        if len(l) > d:
            d = len(l)
            f = l
          
    #print(m)  
    p = []
    for i in range(k):
        l = m.__getitem__(indices[i])
        if len(l) == d:
           l.sort()
           p.append(l)
    
    r = len(p)
    #print(f"r = {r}, k = {k}")
    
    if r == k :
        #print("max everywhere")
        p = [p.pop(0)]   
        
    if len(p) == 1:
        f = p[0] 
        return [f[0]*5,f[-1]*5+3]
    
    #print(p)
    
    v = p[0]
    for i in range(1,r):
        v = fusion(v,p[i])
        
    f = v
    return [f[0]*5,f[-1]*5+3]
 
# fusionne deux listes  
def fusion(l:list,t:list):

    p = l
    for i in t :
        if p.count(i) == 0:
            p.append(i)
            
    return p
                  
# supprime la sous chaine d'indice de debut start jusqu'a end dans une chaine de caractere              
def remove_index(string:str,start:int,end:int):
    
    n = len(string)
    block = ''
    
    for i in range(0,start):
        block+=string[i]
    
        
    for i in range(end+1,n):
        block+=string[i]
    
    if block == '0':
        return '::'
    
   
    b = ':'
    
    if block[0] ==':':
        b+=block
        return b 
    
    if end == n-1:
        block+=':'
     
    return block
 
# compress une addresse ipv6       
def compress_ipv6_address(ipv6_address:str):
    
    index = zero_indices(ipv6_address)
    #print(index)
    ip_address =  remove_index(ipv6_address,index[0],index[1])
    return  remove_leading_zeros(ip_address)

# decompress une addresse ipv6
def decompress_ipv6_address(ipv6_address:str):
    
    block = '0000:' 
    blocks = ipv6_address.split('::')
    n = len(blocks)
    
    start = blocks[0]
    end = blocks[-1]
    
   
    number_blocks = 8 - len(end.split(':')) - len(start.split(':'))
    
    if start != '':
        start+=':'
    
    for i in range(number_blocks):
        start += block
        
    start+=end
    
    return add_leading_zeros(start)

# affiche le menu
def menu():
    print(f"""
      +--------------------+ Algorithme de compression & decompression d'adresse ipv6 +--------------------+\n \
     +----------------------------------------------------------------------------------------------------+\n\
      +-1) Compresser une adresse ipv6                                                                     +\n\
      +-2) Decompresser une adresse ipv6                                                                   +\n\
      +-0) Quitter                                                                                         +\n\
      ++--------------------------------------------------------------------------------------------------++\n\
      \
      """)

menu()

choice  = int(input("Entrer votre choix  : "))

while choice != 0 :
    
    if choice == 1 :
        print("\n")
        ipv6_address = input("Entrer l'adresse ipv6 : ")
        print("+--------------------------------------------------------------+")
        print(f"+adresse compresser : {compress_ipv6_address(ipv6_address)}  +")
        print("+--------------------------------------------------------------+")
    elif choice == 2 :
        print("\n")
        ipv6_address = input("Entrer l'adresse ipv6 : ")
        print("+--------------------------------------------------------------+")
        print(f"+adresse decompresser : {decompress_ipv6_address(ipv6_address)}+")
        print("+--------------------------------------------------------------+")
    elif choice == 0:
        print("\n")
        print("FIN ...")
    
    menu()
    choice  = int(input("Entrer votre choix  : "))

print("FIN ...")   


    
                




